#Import library
from json import load
from difflib import get_close_matches

#Loading the json data as python dictionary
data = load(open("dictionary.json"))

def retrieve_definition(word):
    #Removing the case-sensitivity from the program
    #For example 'Rain' and 'rain' will give same output
    #Converting all letters to lower because out data is in that format
    word = word.lower()

    #Check for non existing words
    #1st elif: To make sure the program return the definition of words that start with a capital letter (e.g. Delhi, Texas)
    #2nd elif: To make sure the program return the definition of acronyms (e.g. USA, NATO)
    if word in data:
        return data[word]
    # if word.lower() in data:
        # return data[word.lower()]
    elif word.title() in data:
        return data[word.title()]
    elif word.upper() in data:
        return data[word.upper()]
    #3rd elif: To find a similar word
    #-- len > 0 because we can print only when the word has 1 or more close matches
    #-- In the return statement, the last [0] represents the first element from the list of close matches
    elif len(get_close_matches(word, data.keys())) > 0:
        answer = input("Did you mean %s instead? [y or n]: " % get_close_matches(word,data.keys())[0])
        if answer == "y":
            return data[get_close_matches(word,data.keys())[0]]
        elif answer == "n":
            return("The word doesn't exist, yet.")
        else:
            return("We don't understand your entry. Apologies.")
    else:
        return("The word doesn't exist. Please double check it.")

required_word = input("Enter a word: ")

#Retrive the definition using function and print the result
output = retrieve_definition(required_word)

#If a word has more than one definition, print them recursively
if type(output) == list:
    for item in output:
        print("-", item)
#For words having single definition
else:
    print("-", output)
